COL_RED="\033[0;31m"
COL_GRN="\033[0;32m"
COL_END="\033[0m"

REPO=docker-to-linux

.PHONY:
debian: debian.img

.PHONY:
ubuntu: ubuntu.img

.PHONY:
alpine: alpine.img

.PHONY:
gentoo: gentoo.img

%.tar:
	@echo ${COL_GRN}"[Dump $* directory structure to tar archive]"${COL_END}
	podman build -f $*/Dockerfile -t ${REPO}/$* .
	podman export -o $*.tar `podman run -d ${REPO}/$* /bin/true`

%.dir: %.tar
	@echo ${COL_GRN}"[Extract $* tar archive]"${COL_END}
	mkdir -p $*.dir
	tar -xvf $*.tar -C $*.dir

%.img: builder %.dir
	@echo ${COL_GRN}"[Create $* disk image]"${COL_END}
	podman run -it \
		-v `pwd`:/os:rw \
		-e DISTR=$* \
		--privileged \
		${REPO}/builder bash /os/create_image.sh

.PHONY:
builder:
	@echo ${COL_GRN}"[Ensure builder is ready]"${COL_END}
	@if [ "`podman images -q ${REPO}/builder`" = '' ]; then\
		podman build -f Dockerfile -t ${REPO}/builder .;\
	fi

.PHONY:
builder-interactive:
	podman run -it \
		-v `pwd`:/os:rw \
		--cap-add SYS_ADMIN \
		${REPO}/builder bash

.PHONY:
clean: clean-docker-procs clean-docker-images
	@echo ${COL_GRN}"[Remove leftovers]"${COL_END}
	rm -rf mnt debian.* alpine.* ubuntu.*

.PHONY:
clean-docker-procs:
	@echo ${COL_GRN}"[Remove Docker Processes]"${COL_END}
	@if [ "`podman ps -qa -f=label=com.iximiuz-project=${REPO}`" != '' ]; then\
		podman rm `podman ps -qa -f=label=com.iximiuz-project=${REPO}`;\
	else\
		echo "<noop>";\
	fi

.PHONY:
clean-docker-images:
	@echo ${COL_GRN}"[Remove Docker Images]"${COL_END}
	@if [ "`podman images -q ${REPO}/*`" != '' ]; then\
		podman rmi `podman images -q ${REPO}/*`;\
	else\
		echo "<noop>";\
	fi

